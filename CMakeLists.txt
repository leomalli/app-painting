cmake_minimum_required( VERSION 3.0 )
project( application )

# cpp flags
SET( CMAKE_CXX_STANDARD 17 )
SET( COMPILE_FLAGS "-g -Wall -pedantic" )
SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMPILE_FLAGS}" )

# Magic to find OpenCV and wxWidgets
find_package( OpenCV REQUIRED )
find_package( wxWidgets REQUIRED )
include( ${wxWidgets_USE_FILE} )
include_directories( ${OpenCV_INCLUDE_DIRS} )
include_directories( ${CMAKE_SOURCE_DIR} )

# Include our the directories
include_directories( include )

#Grab headers from include
FILE( GLOB INCL_HEADERS include/*.h )

add_subdirectory( include )
add_subdirectory( src )

#Grab src files from src
#FILE( GLOB SRCFILES src/*.cpp )
#add_executable( ${PROJECT_NAME} ${SRCFILES} ${INCL_HEADERS} )
#
#target_link_libraries( ${PROJECT_NAME}
#    PUBLIC ${wxWidgets_LIBRARIES} ${OpenCV_LIBS}
#    PRIVATE Panels PaintAlgorithms LOGGER )


# make run will compile and run the executable
add_custom_target(run
    COMMAND ${PROJECT_NAME}
    DEPENDS ${PROJECT_NAME}
    WORKING_DIRECTORY ${CMAKE_PROJECT_DIR}
)
