# Description

This tool is used to apply "filters" to given images.
It is a compilation of many algorithms that acheive different artistic effect and style (many of which, I recon, are quite questionnable).\
The goal of the project is to read papers on certain algorithms that I find quite beautiful and then implement them here in C++.


# Current implemented effects

-   Oil painting
-   Random ellipses filling
-   Pointillism (currently in development)


# Installation
For the moment I only set up a basic CMake routine that I plan to update to be more user friendly.

[OpenCV 4.5.3](https://opencv.org/releases/) and [wxWidgets 3.0.5](https://www.wxwidgets.org/downloads/) must be installed on your machine beforehand.
These specifics versions are the one installed for me, I know for a fact that the latest dev version of wxWidgets break things, and I still have not tried for OpenCV.


Then clone the repo and cd into it, make sure that CMake is installed and run the following to build and run the project.
```
mkdir build
cd build
cmake ..
make run
```

# Algorithms references

Here are the link to the algorithms I implemented or the link to where I found the inspiration.
If not specified, from me.

### Oil Painting
[The supercomputing Blog](http://supercomputingblog.com/graphics/oil-painting-algorithm/)

### Pointillism Art
[Yanshu Hong & Tiffany Liu](https://web.stanford.edu/class/ee368/Project_Autumn_1516/Reports/Hong_Liu.pdf)

# TODO list

- [X] Add possibility to zoom + navigate ImagePanel
- [X] CMake use `-g -Wall -pedantic` flags when compiling
- [ ] Read through the code and comment it
- [ ] Finish Pointillism algorithm implementation
    - [X] Compute Palette
    - [X] Draw circles with closest color
    - [X] Draw things by cluster of dots
    - [X] Brush form
    - [X] Use gradient to direct brushstrokes
    - [ ] Work on the random color touches
    - [X] See if multithreading is possible
- [ ] ~~Update CMake to DL OpenCV/wxWidgets and build them if not present on user machine~~ -> Lot more involved than planned, out of scope for this project.
- [ ] Pop up asking filename when doing a full render of image
- [ ] Create a way for cMain to handle heavy functions load request from ToolPanel
- [ ] Fix the saving option in ToolPanel so that it doesn't use absolute path (that work only on my machine...)

