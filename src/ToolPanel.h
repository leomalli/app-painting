#pragma once

#include <wx/wx.h>
#include <wx/panel.h>
#include <wx/choicebk.h>
#include <wx/splitter.h>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

#include <tuple>

#include "effectsPanels/PanelOilPainting.h"
#include "effectsPanels/PanelRandomEllipses.h"
#include "effectsPanels/PanelPointillism.h"


class ToolPanel : public wxPanel
{

public:
    ToolPanel(wxSplitterWindow *parent);
    wxSplitterWindow *m_parent = nullptr;

    wxChoicebook *m_choicebook;

    //Used to ease comunication with the algorithms
    wxImage Mat_to_wxImage(cv::Mat &img) const;


    //Functions used to comunicate with ImagePanel
    bool isImageLoaded() const;
    wxString GetImagePath() const;
    std::tuple<int,int> GetLowResDim() const;
    void SetImagePreview(wxImage &preview);
    void SaveImage(cv::Mat &img);

};