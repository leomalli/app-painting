add_subdirectory( algorithms )
add_subdirectory( effectsPanels )

FILE( GLOB SRCFILES *.cpp )
add_executable( ${PROJECT_NAME} ${SRCFILES} ${INCL_HEADERS} )

target_link_libraries( ${PROJECT_NAME}
    PUBLIC ${wxWidgets_LIBRARIES} ${OpenCV_LIBS}
    PRIVATE Panels PaintAlgorithms )
