#include "cMain.h"

#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP

#define ZOOM_REQUEST_ID 10004

/*
We can allocate IDs ranging from 10001 to 10099 included
*/

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
    EVT_MENU(10001, cMain::LoadImage)
    EVT_MENU_RANGE(ZOOM_REQUEST_ID, ZOOM_REQUEST_ID + 2, cMain::SendZoomRequest)
wxEND_EVENT_TABLE()

cMain::cMain() : wxFrame(nullptr, wxID_ANY, "Painting effects", wxPoint(30,30), wxSize(800,600))
{

    // Create menu bar and append some events to it
    m_MenuBar = new wxMenuBar();
    this->SetMenuBar(m_MenuBar);

    wxMenu *menuFile = new wxMenu();
    menuFile->Append(10001, "Open");
    menuFile->Append(10002, "Save");
    menuFile->Append(10003, "Exit");

    m_MenuBar->Append(menuFile, "File");


    // Create zoom control in the menu bar
    wxMenu *menuZoom = new wxMenu();
    menuZoom->Append(ZOOM_REQUEST_ID + ImagePanel::RESET, "Reset");
    menuZoom->Append(ZOOM_REQUEST_ID + ImagePanel::ZOOM, "Zoom (+)");
    menuZoom->Append(ZOOM_REQUEST_ID + ImagePanel::DEZOOM, "Dezoom (-)");

    m_MenuBar->Append(menuZoom, "Zoom Ctrl");

    wxInitAllImageHandlers();


    // Create main sizer and add image and tool panel to it
    wxBoxSizer *m_sizermain = new wxBoxSizer(wxVERTICAL);

    wxSplitterWindow *m_splitter = new wxSplitterWindow(this, wxID_ANY);
    m_splitter->SetMinimumPaneSize(200);
    m_sizermain->Add(m_splitter, 1, wxEXPAND, 0);

    m_imagepanel = new ImagePanel(m_splitter);
    m_toolpanel = new ToolPanel(m_splitter);
    m_splitter->SplitVertically(m_imagepanel, m_toolpanel);

    

    this->Centre();
}


// Is called when user click on the load button
void cMain::LoadImage(wxCommandEvent &evt)
{
    // Open file dialog and get path to it if user didn't cancel
    wxFileDialog openFileDialog(this, "Select Picture", "", "", "*.jpg;*.jpeg;*.png", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if ( openFileDialog.ShowModal() == wxID_CANCEL )
        return;

    wxString path = openFileDialog.GetPath();

    //Set the image panel to the given img path
    m_imagepanel->SetImage(path);

    evt.Skip();
}

// Utility to check if user loaded an image, it communicates with the image panel
bool cMain::isImageLoaded() const
{
    bool res = m_imagepanel->isImageLoaded();
    return res;
}


// Communicates with image panel to get path of currently loaded image
wxString cMain::GetImagePath() const
{
    wxString path = m_imagepanel->GetImagePath();
    return path;
}


// Ask the image panel for the low res dimension of the loaded img
std::tuple<int,int> cMain::GetLowResDim() const
{
    std::tuple<int,int> dim = m_imagepanel->GetLowResDim();
    return dim;
}


// Ask image panel to set some img to the preview panel
void cMain::SetImagePreview(wxImage &preview)
{
    m_imagepanel->SetImagePreview(preview);
}



// Called by cApp on KeyDown event return true when handled correctly, false otherwise
bool cMain::OnKeyDown(int KeyCode)
{
    bool flag = true;
    switch (KeyCode)
    {
    case 76:    // The "l" key
        //Set the image panel
        //TODO: Remove it
        m_imagepanel->SetImage("/home/crapito/coding/app-painting/src/LargeImage.jpg");
        m_imagepanel->SendSizeEvent();
        break;
    case 75:    // The "k" key
        //Set the image panel
        //TODO: Remove it
        m_imagepanel->SetImage("/home/crapito/coding/app-painting/src/Colors.png");
        m_imagepanel->SendSizeEvent();
        break;
    case 90:    // The "z" key
        //Binds to zoom
        m_imagepanel->ZoomImage(ImagePanel::ZOOM);
        break;
    case 45:    // The "-" key
        //Binds to dezoom
        m_imagepanel->ZoomImage(ImagePanel::DEZOOM);
        break;
    case 80:    // The "p" key
        LOG(INFO) << "SENT PAINT";
        m_imagepanel->PaintNow();
        break;
    
    default:
        LOG(INFO) << "Key pressed not handled: " << KeyCode;
        flag = false;
        break;
    }

    return flag;
}


// Send a request to the image panel to zoom
void cMain::SendZoomRequest(wxCommandEvent &evt)
{

    int id = evt.GetId();
    switch (id)
    {
    case ZOOM_REQUEST_ID:
        m_imagepanel->ZoomImage(ImagePanel::RESET);
        break;
    
    case (ZOOM_REQUEST_ID + 1):
        m_imagepanel->ZoomImage(ImagePanel::ZOOM);
        break;
    
    case (ZOOM_REQUEST_ID + 2):
        m_imagepanel->ZoomImage(ImagePanel::DEZOOM);
        break;
    
    default:
        LOG(WARNING) << "Event not recognised in cMain::SendZoomRequest(): " << id;
        break;
    }
    
    evt.Skip();
}


cMain::~cMain()
{

}