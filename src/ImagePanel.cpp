#include <wx/dc.h>
#include <wx/graphics.h>
#include "ImagePanel.h"
#include "easylogging++.h"

// Max width or height of preview image
#define MAX_PIXEL_VALUE 1000

/*
We can allocate IDs ranging from 10100 to 10199 included
*/

wxBEGIN_EVENT_TABLE(ImagePanel, wxPanel)
    EVT_SIZE(ImagePanel::OnSize)
    EVT_PAINT(ImagePanel::PaintEvent)
wxEND_EVENT_TABLE()


ImagePanel::ImagePanel(wxSplitterWindow *parent)
    : wxPanel(parent, wxID_ANY, wxPoint(-1,-1), wxSize(-1,-1), wxBORDER_SUNKEN)
{
    m_parent = parent;

    // Set the panel to be scrollable and define its scroll rate
    m_imagecontainer = new wxScrolled<wxPanel>(this, wxID_ANY);
    m_imagecontainer->SetScrollRate(10, 10);


    // Define default values for some variables
    is_image_loaded = false;
    zoom_factor = 1;
    zoom_increment = 0.2;

    preview_width = -1;
    preview_height = -1;
}


// Take image path and load it up in the app
void ImagePanel::SetImage(const wxString &path)
{
    // Load file and print error if couldn't do so
    if (!src_image.LoadFile(path, wxBITMAP_TYPE_ANY))
    {
        LOG(ERROR) << "Could not load file at: " << path;
        return;
    }
    is_image_loaded = true;
    image_path = path;
    src_width = src_image.GetWidth();
    src_heigth = src_image.GetHeight();

    // Reset zoom factor when new image is loaded
    zoom_factor = 1;

    src_ratio = (float)src_width / (float)src_heigth;

    // If the height or height of img is too large, create a lower version of it and
    // define the according variables
    if ( src_width <= MAX_PIXEL_VALUE && src_heigth <= MAX_PIXEL_VALUE)
    {
        // image is small enough, we just copy it to low_res_img and scaling is then 1
        low_res_image = src_image.Copy();
        scaling_ratio = 1;
    }
    else
    {
        // Image too large, copy it to low_res_img, find out if it is wider or heigher
        low_res_image = src_image.Copy();
        int nw = src_ratio >= 1 ? MAX_PIXEL_VALUE : int( MAX_PIXEL_VALUE * src_ratio);
        int nh = src_ratio <= 1 ? MAX_PIXEL_VALUE : int( float(MAX_PIXEL_VALUE) / src_ratio);

        // Rescale by the defined new width/height
        low_res_image.Rescale(nw, nh);
        scaling_ratio = (float)src_width / (float)nw;
    }

    // After everything is loaded and defined, set the preview img to the low res version
    SetImagePreview(low_res_image);
}


// Set preview img to the given img, send size + paint event to correctly reload panel
void ImagePanel::SetImagePreview(const wxImage &img)
{
    preview_image = img;
    // Reset the zoom factor to 1
    zoom_factor = 1;

    // Send events to panel so that it refreshes everything
    this->SendSizeEvent();
    this->PaintNow();
}


// This redirect painting events to our custom painting function
void ImagePanel::PaintEvent(wxPaintEvent &evt)
{
    this->PaintNow();
    evt.Skip();
}


// Ask the panel to refresh itself
void ImagePanel::PaintNow()
{
    if ( !is_image_loaded )
        return;

    // Create a paint dc from the image container and pass it to the rendering function
    wxPaintDC dc(m_imagecontainer);
    Render(dc);
}


// Take a PaintDC and will render the current state of the class on it
void ImagePanel::Render(wxPaintDC &dc)
{
    // Create Graphical context from the dc and get basic info from it
    double gc_width, gc_height;
    wxGraphicsContext *gc = wxGraphicsContext::Create( dc );
    gc->GetSize(&gc_width, &gc_height);

    // Compute resulting size of image
    preview_width = gc_width * zoom_factor;
    preview_height = gc_height * zoom_factor;

    // Bitmap is size of the gc, update (virtual) size of scroller container
    bit_image = wxBitmap( preview_image.Scale( gc_width, gc_height , wxIMAGE_QUALITY_HIGH ) );
    m_imagecontainer->SetVirtualSize(wxSize(preview_width, preview_height));
    
    // Get relative coord from scroller win and compute scaling + transform matrix to
    // apply to gc
    wxPoint coord = m_imagecontainer->CalcUnscrolledPosition(wxPoint(0,0));
    wxPoint pt = GetNearestDrawable( coord, gc_width, gc_height, preview_width, preview_height);
    gc->Scale(zoom_factor, zoom_factor);
    gc->Translate( -pt.x / zoom_factor, -pt.y / zoom_factor );
    gc->DrawBitmap( bit_image, .0, .0, gc_width, gc_height);
    
    delete gc;
}


// Called on any size event, will update the size of the img container
void ImagePanel::OnSize(wxSizeEvent &evt)
{
    if ( !is_image_loaded )
        return;

    // Get size of the new drawable space
    int h = evt.GetSize().GetHeight();
    int w = evt.GetSize().GetWidth();

    // Determine if the ratio of drawable is larger/smaller that ratio of img
    // then set size of container accordingly (so that it maches ratio of src_img)
    if ( (float)w / (float)h < src_ratio )
    {
        float nh = (float)w / src_ratio;
        m_imagecontainer->SetSize( wxSize(w, (int)nh ) );
    }
    else
    {
        float nw = float(h) * src_ratio;
        m_imagecontainer->SetSize( wxSize((int)nw, h) );
    }

    m_imagecontainer->Center();
    evt.Skip();
}


// Utility used to communicate with other panels
bool ImagePanel::isImageLoaded() const
{
    return is_image_loaded;
}


// Return the low res image dimension (not the shown dimension as it could changed according to
// panel size, zoom, etc. )
std::tuple<int, int> ImagePanel::GetLowResDim() const
{
    return std::tuple<int,int>(low_res_image.GetWidth(), low_res_image.GetHeight());
}


// Return the path of the loaded img
wxString ImagePanel::GetImagePath() const
{
    return image_path;
}


// Change the zoom factor and refresh the panel to send required events
void ImagePanel::ZoomImage(const int &zoom_indicator)
{
    switch (zoom_indicator)
    {
    case RESET:
        zoom_factor = 1;
        break;
    
    case ZOOM:
        zoom_factor += zoom_increment;
        break;
    
    case DEZOOM:
        zoom_factor -= zoom_increment;
        if (zoom_factor < 1)
            zoom_factor = 1;
        break;
    
    default:
        LOG(WARNING) << "ImagePanel::zoomImage(), case not covered: " << zoom_indicator;
        break;
    }

    this->Refresh();
}


// Compute rectangle of size (dc_w, dc_h) with corner as near as coord as possible
// knowing that the bitmap has size (bit_w, bit_h)
wxPoint ImagePanel::GetNearestDrawable(const wxPoint &coord, const int &dc_w, const int &dc_h,
    const int &bit_w, const int &bit_h) const
{
    int x = coord.x, y = coord.y;

    if (x < 0)
        x = 0;
    else if (x + dc_w > bit_w)
        x = bit_w - dc_w;

    if (y < 0)
        y = 0;
    else if (y + dc_h > bit_h)
        y = bit_h - dc_h;

    return wxPoint(x, y);
}