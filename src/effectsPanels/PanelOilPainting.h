#pragma once

#include <wx/wx.h>
#include <wx/panel.h>
#include <wx/choicebk.h>
#include <wx/spinctrl.h>
#include <tuple>



class PanelOilPainting : public wxPanel
{
public:
    PanelOilPainting(wxChoicebook *parent);
    wxChoicebook *m_parent = nullptr;

    wxSpinCtrl *m_radius = nullptr;
    wxSpinCtrl *m_intensityBins = nullptr;
    wxButton *m_buttonPreview = nullptr;
    wxButton *m_buttonRender = nullptr;

public:
    void ComputePreview(wxCommandEvent &evt);
    void ComputeFullRender(wxCommandEvent &evt);

    wxDECLARE_EVENT_TABLE();
};