#pragma once

#include <wx/wx.h>
#include <wx/panel.h>
#include <wx/choicebk.h>
#include <wx/spinctrl.h>
#include <tuple>



class PanelPointillism : public wxPanel
{
public:
    PanelPointillism(wxChoicebook *parent);
    wxChoicebook *m_parent = nullptr;

    wxSpinCtrl *m_radius = nullptr;
    wxSpinCtrl *m_intensity = nullptr;
    wxSpinCtrl *m_transparency = nullptr;
    wxSpinCtrl *m_numberColors = nullptr;
    wxSpinCtrl *m_spacingNumber = nullptr;
    wxSpinCtrl *m_distribMu = nullptr;
    wxSpinCtrl *m_distribSigma = nullptr;
    wxSpinCtrl *m_amountOfComplementary = nullptr;
    wxButton *m_buttonPreview = nullptr;
    wxButton *m_buttonRender = nullptr;

public:
    void ComputePreview(wxCommandEvent &evt);
    void ComputeFullRender(wxCommandEvent &evt);

    wxDECLARE_EVENT_TABLE();
};