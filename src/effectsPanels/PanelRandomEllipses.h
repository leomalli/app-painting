#pragma once

#include <wx/wx.h>
#include <wx/panel.h>
#include <wx/choicebk.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <tuple>



class PanelRandomEllipses : public wxPanel
{
public:
    PanelRandomEllipses(wxChoicebook *parent);
    wxChoicebook *m_parent = nullptr;

    //The controls
    wxSpinCtrl *m_radius = nullptr;
    wxSpinCtrl *m_ellipsesRatio = nullptr;
    wxSpinCtrl *m_numberOfEllipses = nullptr;
    wxCheckBox *m_usegradiant = nullptr;
    wxButton *m_buttonPreview = nullptr;
    wxButton *m_buttonRender = nullptr;


public:
    void ComputePreview(wxCommandEvent &evt);
    void ComputeFullRender(wxCommandEvent &evt);


    wxDECLARE_EVENT_TABLE();
};