#include <chrono>
#include "PanelPointillism.h"
#include "../ToolPanel.h"

#include "../algorithms/Pointillism.h"
#include "easylogging++.h"


/*
We can allocate IDs ranging from 10500 to 10599 included
*/

wxBEGIN_EVENT_TABLE(PanelPointillism, wxPanel)
    EVT_BUTTON(10502, PanelPointillism::ComputePreview)
    EVT_BUTTON(10503, PanelPointillism::ComputeFullRender)
wxEND_EVENT_TABLE()


PanelPointillism::PanelPointillism(wxChoicebook *parent)
    : wxPanel(parent, wxID_ANY, wxPoint(-1,-1), wxSize(-1,-1), wxBORDER_SUNKEN)
{
    m_parent = parent;

    //Create the horizontal sizers
    m_radius = new wxSpinCtrl(this, 10500, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 6);
    wxStaticText *m_radius_text = new wxStaticText(this, wxID_ANY, "Brush radius:");
    wxBoxSizer *sizerRadius = new wxBoxSizer(wxHORIZONTAL);
    sizerRadius->Add(m_radius_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerRadius->Add(m_radius,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_transparency = new wxSpinCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 40);
    wxStaticText *m_transparency_text = new wxStaticText(this, wxID_ANY, "Brush oppacity (%):");
    wxBoxSizer *sizerTransparency = new wxBoxSizer(wxHORIZONTAL);
    sizerTransparency->Add(m_transparency_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerTransparency->Add(m_transparency,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_numberColors = new wxSpinCtrl(this, 10501, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 15, 7);
    wxStaticText *m_numberColors_text = new wxStaticText(this, wxID_ANY, "Number of colors:");
    wxBoxSizer *sizerNumberColors = new wxBoxSizer(wxHORIZONTAL);
    sizerNumberColors->Add(m_numberColors_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerNumberColors->Add(m_numberColors,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_intensity = new wxSpinCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 10);
    wxStaticText *m_intensity_text = new wxStaticText(this, wxID_ANY, "Intensity:");
    wxBoxSizer *sizerIntensity = new wxBoxSizer(wxHORIZONTAL);
    sizerIntensity->Add(m_intensity_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerIntensity->Add(m_intensity,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_spacingNumber = new wxSpinCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 25);
    wxStaticText *m_spacingNumber_text = new wxStaticText(this, wxID_ANY, "Cluster spacing:");
    wxBoxSizer *sizerSpacingNumber = new wxBoxSizer(wxHORIZONTAL);
    sizerSpacingNumber->Add(m_spacingNumber_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerSpacingNumber->Add(m_spacingNumber,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_distribMu = new wxSpinCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 10);
    wxStaticText *m_distribMu_text = new wxStaticText(this, wxID_ANY, "Dispertion mean:");
    wxBoxSizer *sizerDistribMu = new wxBoxSizer(wxHORIZONTAL);
    sizerDistribMu->Add(m_distribMu_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerDistribMu->Add(m_distribMu,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_distribSigma = new wxSpinCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 25);
    wxStaticText *m_distribSigma_text = new wxStaticText(this, wxID_ANY, "Dispertion std deviation:");
    wxBoxSizer *sizerDistribSigma = new wxBoxSizer(wxHORIZONTAL);
    sizerDistribSigma->Add(m_distribSigma_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerDistribSigma->Add(m_distribSigma,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_amountOfComplementary = new wxSpinCtrl(this, 10505, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 5);
    wxStaticText *m_amountOfComplementary_text = new wxStaticText(this, wxID_ANY, "% of complementary: ");
    wxBoxSizer *sizerAmountOfComplementary = new wxBoxSizer(wxHORIZONTAL);
    sizerAmountOfComplementary->Add(m_amountOfComplementary_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerAmountOfComplementary->Add(m_amountOfComplementary,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_buttonPreview = new wxButton(this, 10502, "Preview");
    m_buttonRender = new wxButton(this, 10503, "Full Render");

    wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(sizerRadius, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerTransparency, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerIntensity, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerSpacingNumber, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerNumberColors, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerDistribMu, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerDistribSigma, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerAmountOfComplementary, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(m_buttonPreview, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);
    boxSizer->Add(m_buttonRender, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);

    this->SetSizer(boxSizer);
}


// Called when preview button is pressed
// TODO: Delete the full rander preview and replace it by this one as we do not really compute previews
void PanelPointillism::ComputePreview(wxCommandEvent &evt)
{
    // Check is some image is loaded, otherwise abort
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    const bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";
        return;
    }
    
    // Get all the necessarry variables from panels
    const wxString path = toolpanel->GetImagePath();
    const int   radius = m_radius->GetValue(),
                numberOfCluster = m_numberColors->GetValue(),
                spacingNumber = m_spacingNumber->GetValue(),
                intensity = m_intensity->GetValue(),
                mu = m_distribMu->GetValue(),
                sigma = m_distribSigma->GetValue();
    const float transparency = (float)m_transparency->GetValue() / 100.0f; // So we get a %

    // Start timer
    auto start = std::chrono::steady_clock::now();

    // Lauch computation
    cv::Mat image_preview_cv =
        Pointillism(path.ToStdString(), radius, transparency, spacingNumber,
                    numberOfCluster, intensity, mu, sigma);

    // End timer and log infos about the call
    auto end = std::chrono::steady_clock::now();
    LOG(INFO)   << "Pointillism computation with:\n"
                << "Radius: " << radius
                << ", number of colors: " << numberOfCluster
                << ", spacing number: " << spacingNumber
                << ", transparency: " << transparency
                << ", intensity: " << intensity
                << ", mu: " << mu
                << ", sigma: " << sigma
                << "\nDone in: " << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << " s";


    // Get the preview to the correct format and show it
    wxImage image_preview = toolpanel->Mat_to_wxImage(image_preview_cv);
    toolpanel->SetImagePreview(image_preview);

    evt.Skip();
}


void PanelPointillism::ComputeFullRender(wxCommandEvent &evt)
{
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";

        return;
    }

    //TODO
    //      Ask where to save the file and compile it (and show it in the panel)

    LOG(INFO) << "TODO: FULL RENDERING";
    /*
    wxString path = toolpanel->GetImagePath();
    int radius = m_radius->GetValue(), numberOfCluster = m_numberColors->GetValue(), spacingNumber = m_spacingNumber->GetValue(),
        amountOfComplementary = m_amountOfComplementary->GetValue();

    cv::Mat rendered_img = Pointillism( path.ToStdString(), radius, spacingNumber,
        numberOfCluster, amountOfComplementary);
        

    toolpanel->SaveImage(rendered_img);
    */

    evt.Skip();
}