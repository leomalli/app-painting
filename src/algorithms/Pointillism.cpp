#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>
#include <tuple>
#include <random>
#include <algorithm>
#include <math.h>

#include "easylogging++.h"
#include "Pointillism.h"


/********************************************************************
*********************************************************************
****                                                             ****
****   This algorithm is an implementation of the paper:         ****
****      "Create Pointillism Art from Digital Images"           ****
****               Yanshu Hong & Tiffany Liu                     ****
****                                                             ****
*********************************************************************
********************************************************************/

//Used to preprocess img i.e. low pass filter + downsample + bluring
#define MAX_DIM 500    //downsample to this size if larger
#define GAMMA 1.8       //Gamma correction for intensity map
#define BRUSHMASKPRECISION 400  //Precision of the original brush stroke mask
#define MAX_DOT_PER_CLUSTER 100 //Max number of dots a cluster can have


//Main program
cv::Mat Pointillism(const std::string& path, const int &radius, const float &transparency,
    const int &clust_dist, const int &numberOfColors, const int &alpha, const int &mu, const int &sigma)
{

    srand(static_cast <unsigned> (time(0)));
    cv::Mat input_img;
    input_img = cv::imread(path);

    preprossessing(input_img, 1.0f);

    std::vector<cv::Vec3f> color_palette = computePalette(input_img, numberOfColors);

    // Compute intensity map
    cv::Mat intensity_map;
    cv::cvtColor( input_img, intensity_map, cv::COLOR_BGR2GRAY );
    // Gamma correction
    cv::Mat lookUpTable(1, 256, CV_8U);
    uchar* p = lookUpTable.ptr();
    for ( int i = 0; i < 256; i++ )
    {
        p[i] = cv::saturate_cast<uchar>( cv::pow(i / 255.0, GAMMA ) * 255.0);
    }
    cv::LUT( intensity_map, lookUpTable, intensity_map );

    // Invert the intensity map
    cv::bitwise_not( intensity_map, intensity_map );


    // Create the brush mask and resize it
    cv::Mat brushMask;
    createBrushStroke(brushMask);
    const float downsizeBrushRatio = (float)(2*radius + 1) / (float)brushMask.cols;
    cv::resize(brushMask, brushMask, cv::Size(), downsizeBrushRatio, downsizeBrushRatio, cv::INTER_AREA);

    // Create canvas with right dimension, pixels in original: ### -> # - - - # - - - #
    //                                   Each get separated by a       <----->
    //                                   distance d in the out canvas.   d
    cv::Mat canvas( input_img.rows * (1 + clust_dist ) - clust_dist,
                    input_img.cols * (1 + clust_dist ) - clust_dist,
                    CV_8UC4, cv::Scalar(255, 255, 255, 255) );


    // Create the random number generators
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<float> gauss(mu, sigma);
    std::uniform_int_distribution<int> unif(0, color_palette.size() - 2);



    // Loop the pixels
    typedef cv::Point3_<uint8_t> Pixel;

    input_img.forEach<Pixel>([&](Pixel &pix, const int position[]) -> void {

            int i = position[1], j = position[0];
            // access pixel color and intensity
            cv::Vec3f pixel(pix.x, pix.y, pix.z);
            float pixel_intensity = (float)intensity_map.at<uchar>(j, i) / 255.0f;

            // Retrieve the two closest colors
            cv::Matx33f MatColors;
            getMatchingColors(pixel, color_palette, MatColors, gen, unif);


            // TODO: Try to Compute this thing in the HSV space
            // Leasts square to finde closest linear combination to obtain right color
            cv::Matx31f h;
            cv::solve(MatColors, pixel, h, cv::DECOMP_SVD);
            h = h * alpha * pixel_intensity / cv::sum(h)[0];
            cv::max(h, 0, h);
            cv::min(h, MAX_DOT_PER_CLUSTER, h);


            // Equivalent coordonates in destination canvas
            int x = i * (1+clust_dist), y = j * (1+clust_dist);

            // Loop through the colors and print on the canvas
            for (int k = 0; k < 3; k++)
            {
                for (int n = 0; n < (int) h(k); n++)
                {
                    int dx = std::round(gauss(gen)), dy = std::round(gauss(gen));
                    paintBrushStroke(canvas, cv::Point(x + dx, y + dy),
                        cv::Scalar(MatColors(0, k), MatColors(1, k), MatColors(2, k), transparency),
                        brushMask);
                }
            }
    });


    cv::resize(canvas, canvas, cv::Size(), .5, .5);
    cv::imwrite("CANVAS.jpg", canvas);
    cv::cvtColor(canvas, canvas, cv::COLOR_BGRA2BGR);

    return canvas;
}

// Take img -> Low Pass filter (Gaussian filter) -> Resize
// TODO: Maybe further bluring for artistic effect
void preprossessing(cv::Mat &img, float downsampleFactor)
{
    cv::GaussianBlur(img, img, cv::Size(5,5), 1);

    //Rescale img if too large
    if ( img.rows > MAX_DIM || img.cols > MAX_DIM )
    {
        float ratio = (float)img.cols / (float)img.rows;
        int nw = (ratio >= 1.0f) ? MAX_DIM : int(MAX_DIM * ratio);
        int nh = (ratio <= 1.0f) ? MAX_DIM : int( float(MAX_DIM) / ratio);

        cv::resize(img, img, cv::Size(nw, nh));
    }

}

// Take destination img, coordinate, color, and brush mask and paint using the mask
void paintBrushStroke(cv::Mat &dst, const cv::Point &coord, const cv::Scalar &color,
    const cv::Mat &brushMask)
{
    const cv::Point pt = coord - (cv::Point(brushMask.cols - 1, brushMask.rows - 1) / 2);
    const cv::Size sz = brushMask.size();
    
    // TODO: Make this cleaner, do not abbort if to close to the edge
    if (pt.x < 0 || pt.y < 0 || pt.x + sz.width > dst.cols || pt.y + sz.height > dst.rows) return;

    cv::Mat brush;
    cv::multiply(brushMask, color, brush);
    
    // create rectangle around where we want to overlay to speed up computation
    cv::Mat roi = dst(cv::Rect(pt, sz));
    overlayImage(&roi, &brush, cv::Point());
}

// Take image as matrix and number of K-cores and use K-means clustering to compute color palette
std::vector<cv::Vec3f> computePalette(const cv::Mat &img, const int &KCores)
{
    // Flatten the image to apply cv::kmeans
    cv::Mat data;
    img.convertTo(data, CV_32F);
    data = data.reshape(1, data.total());

    // Compute K-Means and store them in cv::Mat centers
    cv::Mat labels, centers;
    cv::kmeans(data, KCores, labels,
        cv::TermCriteria( cv::TermCriteria::EPS+cv::TermCriteria::COUNT, 10, 1.0),
        3, cv::KMEANS_PP_CENTERS, centers);
    centers = centers.reshape(3, centers.rows);

    // Compute palette from centers
    std::vector<cv::Vec3f> palette;
    palette.reserve(KCores);
    for (int i = 0; i < centers.rows; i++)
    {
        palette.emplace_back( centers.at<cv::Vec3f>(i,0));
    }

    // TODO: Ask user if he wants it or not
    // Artistic augmentation of palette
    augmentPalette(palette);
    
    // Full palette with complementary colors added
    std::vector<cv::Vec3f> full_palette;
    full_palette.reserve(palette.size() * 2);
    addComplementaryColors( palette, full_palette );

    return palette;
}

// Takes the color palette and pixel as input and destination array, returns array of two closest colors in palette
// plus a random one.   Destination array must be of length 3
void getMatchingColors(const cv::Vec3f& in_pixel, const std::vector<cv::Vec3f> &in_palette, cv::Matx33f &outMat, std::mt19937 &twister, std::uniform_int_distribution<int> &d)
{
    // Compute dist to each color in the palette
    std::vector<float> distances;
    distances.reserve( in_palette.size() );
    for (auto col : in_palette )
    {
        // TODO: May be some speed up to be done here
        distances.emplace_back( cv::norm( col - in_pixel ) );
    }

    // get index of two min elements
    const auto [ c1, c2 ] = idxOfTwoSmallest( distances );

    int c3 = d(twister);
    if (c3 >= std::min(c1, c2))
        c3++;
    if (c3 >= std::max(c1, c2))
        c3++;


    // Place the colors in dst array (3x2) 
    // |c1(B) c2(B) c3(B)|
    // |c1(G) c2(G) c3(G)|
    // |c1(R) c2(R) c3(R)|
    outMat(0,0) = in_palette[c1][0];
    outMat(1,0) = in_palette[c1][1];
    outMat(2,0) = in_palette[c1][2];

    outMat(0,1) = in_palette[c2][0];
    outMat(1,1) = in_palette[c2][1];
    outMat(2,1) = in_palette[c2][2];

    outMat(0,2) = in_palette[c3][0];
    outMat(1,2) = in_palette[c3][1];
    outMat(2,2) = in_palette[c3][2];
}



// Used to return the argmin of the two smallest elements in our vector
std::tuple< int, int > idxOfTwoSmallest( const std::vector<float> &vec )
{
    // Dist between two pixels cannot be larger
    const int N = 195075;
    float first = N, second = N;
    int idx_f, idx_s, i = 0;
    for ( auto x : vec )
    {
        if ( x < first )
        {
            second = first;
            idx_s = idx_f;
            first = x;
            idx_f = i;
        }
        else if ( x < second )
        {
            second = x;
            idx_s = i;
        }
        i++;
    }


    return std::tuple<int, int>(idx_f, idx_s);
}

// Augment palette by brighting it up and saturating it
void augmentPalette( std::vector<cv::Vec3f> &palette)
{
    for ( auto &col : palette )
    {
        // Convert to HSV
        col = BGR2HSV( col );

        // Add saturation and brightness
        col[1] = ( std::pow(col[1], 0.75f) + 0.05f );
        col[2] = ( std::pow(col[2], 0.75f) + 0.05f );
        col[1] = ( col[1] > 1 ) ? 1 : col[1];
        col[2] = ( col[2] > 1 ) ? 1 : col[2];

        // Convert back to BGR
        col = HSV2BGR( col );
    }
}

// Create the brush stroke mask at a higher resolution using Lammé curves with parameters
// a, b, n
void createBrushStroke(cv::Mat &dst)
{

    const int precision = BRUSHMASKPRECISION;
    const float a = .35f,
                b = 1.0f,
                n = 3.0f;


    std::vector<std::vector<uint8_t>> stroke;
    for (int i = 0; i < 2*precision + 1; i++)
    {
        std::vector<uint8_t> C;
        for (int j = 0; j < 2*precision + 1; j++)
        {
            float x = (i-precision) / (float)precision, y = (j-precision) / (float)precision;
            if (std::pow(std::abs(x / a),n) + std::pow(std::abs(y / b), n) <= 1)
            {
                C.push_back(1);
                C.push_back(1);
                C.push_back(1);
                C.push_back(255);
            }
            else
            {
                C.push_back(0);
                C.push_back(0);
                C.push_back(0);
                C.push_back(0);
            }

        }
        stroke.push_back(C);
    }
    uint8_t data[(2*precision + 1) * (2*precision + 1) * 4];

    int idx=0;
    for (auto col: stroke)
    {
        for (auto p: col)
        {
            data[idx++] = p;
        }
    }
    // Create brush from data
    cv::Mat brush(2*precision + 1, 2*precision + 1, CV_8UC4, data);

    // Cleanup the borders
    dst = brush(cv::Rect(cv::Point(0,precision/2), cv::Size(2*precision + 1, precision)));
}


// Take colors from "in", and add their complementaries in "out", out.size() = in.size() * 2
void addComplementaryColors( const std::vector<cv::Vec3f> &in, std::vector<cv::Vec3f> &out )
{
    for ( auto col : in )
    {
        // push the original color
        out.emplace_back( col );

        // Convert to HSV and shift Hue by random angle in (0, 180) to obtain complementary
        col = BGR2HSV( col );
        col[0] += static_cast<float> ( rand() ) / ( static_cast<float> (RAND_MAX / 180.0f) );
        col[0] = fmodf( col[0], 360.0f );
        out.emplace_back( HSV2BGR(col) );
    }
}


// Simple BGR -> HSV implementation
// BGR color taken from [0, 255], HSV given are [0,360]x[0,1]x[0,1]
cv::Vec3f BGR2HSV( const cv::Vec3f &color )
{
    cv::Vec3f col = color / 255.0f;
    float max = std::max( {col[0], col[1], col[2]} );
    float min = std::min( {col[0], col[1], col[2]} );
    float delta = max - min;

    //  Compute hue
    float H;
    if ( delta == 0)
    {
        H = 0;
    }
    else if ( max == col[2] )
    {
        H = 60.0f * fmodf( (col[1] - col[0]) / delta, 6.0f );
    }
    else if ( max == col[1] )
    {
        H = 60.0f * ( (col[0] - col[2]) / delta + 2 );
    }
    else
    {
        H = 60.0f * ( ( col[2] - col[1] ) / delta + 4);
    }
    H = ( H < 0 ) ? H + 360 : H;

    // Compute Saturation
    float S = (max == 0) ? 0 : ( delta / max );

    return cv::Vec3f(H, S, max);
}

// Simple HSV -> BGR implementation
// HSV taken in [0,360]x[0,1]x[0,1], BGR back in [0,255]
cv::Vec3f HSV2BGR( const cv::Vec3f &color )
{
    float C = color[2] * color[1];
    float X = C * ( 1 - std::abs( fmodf(color[0] / 60.0f, 2.0f) - 1 ) );
    float m = color[2] - C;
    cv::Vec3f final;

    float H = color[0];
    if (H < 60)
    {
        final = {.0f, X, C};
    }
    else if (H < 120)
    {
        final = {.0f, C, X};
    }
    else if (H < 180)
    {
        final = {X, C, .0f};
    }
    else if (H < 240)
    {
        final = {C, X, .0f};
    }
    else if (H < 300)
    {
        final = {C, 0.0f, X};
    }
    else
    {
        final = {X, 0.0f, C};
    }
    final[0] = final[0] + m;
    final[1] = final[1] + m;
    final[2] = final[2] + m;
    return final*255.0f;
}


// Overlays two png images according to their alpha values
void overlayImage( cv::Mat *src, cv::Mat *overlay, const cv::Point &location)
{
    for (int y = std::max(location.y, 0); y < src->rows; y++)
    {
        int fY = y - location.y;

        if (fY >= overlay->rows)
            break;

        for (int x = std::max(location.x, 0); x - src->cols; x++)
        {
            int fX = x - location.x;

            if (fX >= overlay->cols)
                break;

            double opacity = ((double)overlay->data[fY * overlay->step + fX * overlay->channels() + 3]) / 255;

            for (int c = 0; opacity > 0 && c < src->channels(); c++)
            {
                uchar overlayPx = overlay->data[fY * overlay->step + fX * overlay->channels() + c];
                uchar srcPx = src->data[y * src->step + x * src->channels() + c];

                src->data[y * src->step + src->channels() * x + c] = srcPx * (1. - opacity) + overlayPx * opacity;
            }
        }

    }
}