#pragma once

#include <opencv2/imgcodecs.hpp>
#include <random>

void preprossessing(cv::Mat &input, float downsampleFactor);
cv::Mat Pointillism(const std::string& path, const int &radius, const float &transparency,
    const int &spacingNumber, const int &numberOfColors, const int &alpha, const int &mu, const int&sigma);
std::vector<cv::Vec3f> computePalette(const cv::Mat &img, const int &KCores);
void augmentPalette( std::vector<cv::Vec3f> &palette);
void addComplementaryColors( const std::vector<cv::Vec3f> &in, std::vector<cv::Vec3f> &out );
void getMatchingColors(const cv::Vec3f& in_pixel, const std::vector<cv::Vec3f> &in_palette, cv::Matx33f &outMat, std::mt19937 &twister, std::uniform_int_distribution<int> &d);
void createBrushStroke(cv::Mat &dst);
void paintBrushStroke(cv::Mat &dst, const cv::Point &coord, const cv::Scalar &color, const cv::Mat &brushMask);
std::tuple< int, int > idxOfTwoSmallest( const std::vector<float> &vec );
cv::Vec3f BGR2HSV( const cv::Vec3f &color );
cv::Vec3f HSV2BGR( const cv::Vec3f &color );
void overlayImage( cv::Mat *src, cv::Mat *overlay, const cv::Point &location);