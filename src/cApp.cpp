#include "cApp.h"

wxIMPLEMENT_APP(cApp);

cApp::cApp()
{

}


cApp::~cApp()
{

}


bool cApp::OnInit()
{
    m_frame1 = new cMain();
    m_frame1->Show();


    return true;
}

int cApp::FilterEvent(wxEvent &evt)
{
    //  This will catch KeyDown event
    //  TODO: Check if we cannot get combinaison of key instead of just which key is down
    if (evt.GetEventType() == wxEVT_KEY_DOWN)
    {
        // Returns false if event hasn't been handled, then use default filter
        if ( !(m_frame1->OnKeyDown( ((wxKeyEvent&)evt).GetKeyCode())) )
            return wxApp::FilterEvent(evt);
    }

    return wxApp::FilterEvent(evt);
}