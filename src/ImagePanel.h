#pragma once

#include <wx/wx.h>
#include <wx/panel.h>
#include <wx/splitter.h>
#include <tuple>


class ImagePanel : public wxPanel
{
public:
    ImagePanel(wxSplitterWindow *parent);
    //Function used to cumunicate with cMain
    std::tuple<int, int> GetLowResDim() const;
    wxString GetImagePath() const;
    bool isImageLoaded() const;
    void ZoomImage(const int &zoom_indicator);
    void SetImage(const wxString &path);
    void PaintNow();
    void SetImagePreview(const wxImage &img);

    enum ZOOMS_INFOS { RESET = 0, ZOOM, DEZOOM };

    wxDECLARE_EVENT_TABLE();

private:
    wxSplitterWindow *m_parent = nullptr;
    wxBoxSizer *m_boxsizer = nullptr;
    wxScrolled<wxPanel> *m_imagecontainer = nullptr;
    wxString image_path;
    wxImage src_image, low_res_image, preview_image;
    wxBitmap bit_image;

    bool is_image_loaded;
    // Contains the src image width/height (defined on load) and its ratio
    int src_width = -1, src_heigth = -1;
    float src_ratio;

    // Width and Height of the actual preview img, i.e. not of the drawable panel
    int preview_width, preview_height;
    //scaling ratio is the ratio of img_width/low_res_img_width
    float scaling_ratio; 

    // Determine how zoomed we want the image, and by how much it zooms when prompted to
    float zoom_factor, zoom_increment;
    


    void PaintEvent(wxPaintEvent &evt);
    void Render(wxPaintDC &dc);
    void OnSize(wxSizeEvent &evt);
    wxPoint GetNearestDrawable(const wxPoint &coord,
        const int &dc_width, const int &dc_height,
        const int &bit_width, const int &bit_height) const;

};